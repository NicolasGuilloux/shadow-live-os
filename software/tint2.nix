{ config, pkgs, ... }:

let configFile = ../assets/tint2_config;
in {
  # Packages
  environment.systemPackages = with pkgs; [ tint2 ];

  # Start Tint2 at startup
  openbox.autostartLines = [ "${pkgs.tint2}/bin/tint2 -c ${configFile}" ];
}
