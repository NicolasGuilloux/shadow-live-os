{ config, pkgs, lib, ... }:

let
  inherit (import ./utilities/scanner.nix { inherit lib; }) importNixFolder;

  home-manager = builtins.fetchGit {
    url = "https://github.com/nix-community/home-manager.git";
    ref = "0399839271d51d215430a2a511813c7dd2570769";
  };

  envChannel = (builtins.getEnv "SHADOW_CHANNEL");
  shadowChannel = (if envChannel != "" then envChannel else "prod");
in {
  imports = [ (import "${home-manager}/nixos") ]
    ++ (importNixFolder ./hardware) ++ (importNixFolder ./software)
    ++ (importNixFolder ./miscellaneous);

  # Define the channel used from the argument
  programs.shadow-client.channel = shadowChannel;

  # Base version
  system.stateVersion = "20.03";
}
