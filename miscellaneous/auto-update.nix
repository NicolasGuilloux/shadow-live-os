{ config, pkgs, ... }:

{
    # This service is not ready, it produces an error since no configuration is given to the ISO
    systemd.services.nixos-upgrade = {
        enable = false;
        description = "NixOS Upgrade";
        after = [ "network.service" ];
        wantedBy = [ "multi-user.target" ];

        restartIfChanged = false;
        unitConfig.X-StopOnRemoval = false;
        serviceConfig.Type = "oneshot";

        environment = config.nix.envVars // {
          inherit (config.environment.sessionVariables) NIX_PATH;
          HOME = "/root";
        } // config.networking.proxy.envVars;

        path = with pkgs; [
          coreutils
          gnutar
          xz.bin
          gzip
          gitMinimal
          config.nix.package.out
        ];

        script = "${config.system.build.nixos-rebuild}/bin/nixos-rebuild switch -I nixpkgs=https://nixos.org/channels/nixos-20.09-small/nixexprs.tar.xz";
    };
}